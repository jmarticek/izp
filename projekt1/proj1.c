/*

        Projekt 1 - práca s textom
        meno: Juraj Martiček
        login: xmarti97
        (Premium solution)

*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define STRLEN 101

// check for match: if number == number or number == representing char to that number
int isMatching (char currentInputNumber, char currentRecordChar);

// core find function - finds whether the input number (in argument) matches the entity in parameter
int find (char *argv[], char *entity);

// throws error and exits program
int throwError ();

// handles input data - normalize for finding, and format for printing
void handler (char *name, char *number, char *printString);

// validates input arguments
int validate (int argc, char *argv[]);

int main (int argc, char *argv[]) {
  bool hasPrinted = false;
  // return error if validation failed
  if(!validate(argc, argv)) return throwError();
  // load 1 record for check
  char number[STRLEN] = {0};
  char name[STRLEN] = {0};
  while (fgets(name, STRLEN, stdin) && fgets(number, STRLEN, stdin)) {
    char printString[STRLEN * 2];
    // normalize and prepare record for printing
    handler(name, number, printString);
    // if no arguments -> print immediately
    if(argc == 1) {
      hasPrinted = true;
      printf("%s", printString);
      continue;
    }
    // find name match
    if(find(argv, name)) {
      printf("%s", printString);
      hasPrinted = true;
    }
    // else find number match
    else if(find(argv, number)) {
      printf("%s", printString);
      hasPrinted = true;
    }
  }
  if(!hasPrinted) {
    printf("Not found\n");
  }
  return 0;
}

int isMatching (char currentInputNumber, char currentRecordChar) {
  char translateArr[10][4] = {
    {'+'}, // 0
    {'\0'}, // 1
    {'a', 'b', 'c'}, //2
    {'d', 'e', 'f'}, //3
    {'g', 'h', 'i'}, //4
    {'j', 'k', 'l'}, //5
    {'m', 'n', 'o'}, //6
    {'p', 'q', 'r', 's'}, //7
    {'t', 'u', 'v'}, //8
    {'w', 'x', 'y', 'z'} //9
  };
  for(int i = 0; i < 4; i++) {
    if(currentRecordChar == '\0') break;
    if(translateArr[currentInputNumber - '0'][i] == currentRecordChar 
        || currentInputNumber == currentRecordChar) {
      return 1;
    }
  }
  return 0;
}

// entity == name || number
int find (char *argv[], char *entity) {
  int inputNumberLength = strlen(argv[1]);
  int entityLength = strlen(entity);
  int foundCount = 0;
  // iterate over each character in entity and find whether the 2 characters on any position match
  for(int i = 0; i < entityLength; i++) {
    if(isMatching(argv[1][foundCount], entity[i])) foundCount++;
    if(foundCount == inputNumberLength) return 1;
  }
  return 0;
}

int validate (int argc, char *argv[]) {
  // check number of arguments (only 1 allowed)
  if(argc > 2) return 0;
  // check whether there are only numbers in argument
  if(argc == 2) {
    int argLength = strlen(argv[1]);
    for(int i = 0; i < argLength; i++) {
      if(!isdigit(argv[1][i])) return 0;
    }
  }
  return 1;
}

void handler (char *name, char *number, char *printString) {
  int nameLength = strlen(name);
  int numberLength = strlen(number);
  // normalize -> remove newlines
  name[nameLength - 1] = '\0';
  number[numberLength - 1] = '\0';
  // prepare the string for printing
  sprintf(printString, "%s, %s\n", name, number);
  // normalize -> name to lowercase
  for(int i = 0; i < nameLength; i++) name[i] = tolower(name[i]);
}

int throwError () {
  // print error to stderr
  fprintf(stderr, "Neplatny vstup!\n");
  return -1;
}
